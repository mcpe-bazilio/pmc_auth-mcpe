<?php


namespace PMC_Auth\Events;

use PMC_Auth\PMC_Auth;
use pocketmine\event\Cancellable;
use pocketmine\event\plugin\PluginEvent;
use pocketmine\OfflinePlayer;
use pocketmine\Player;
use pocketmine\plugin\Plugin;

class UnregisterEvent extends PluginEvent implements Cancellable {

	public static $handlerList = null;

	/** @var Player|OfflinePlayer|string $player */
	private $player;

	/**
	 * @param Player $player
	 * @param Plugin $Plugin
	 */
	public function __construct($player, $Plugin){
		parent::__construct($Plugin);
		$this->player = $player;
	}

	/**
	 * Get event player
	 *
	 * @return Player|OfflinePlayer|string
	 */
	public function getPlayer(){
		return $this->player;
	}

	/**
	 * Set cancelled message
	 *
	 * @param string $message
	 */
	public function setCancelledMessage($message){
		PMC_Auth::getAPI()->canc_message = $message;
	}
}
