<?php


namespace PMC_Auth\Events;

use PMC_Auth\PMC_Auth;
use pocketmine\event\Cancellable;
use pocketmine\event\plugin\PluginEvent;
use pocketmine\OfflinePlayer;
use pocketmine\Player;
use pocketmine\plugin\Plugin;

class PasswordChangeEvent extends PluginEvent implements Cancellable {

	public static $handlerList = null;

	/** @var Player|OfflinePlayer $player */
	private $player;

	/** @var $password */
	private $password;

	/**
	 * @param Player $player
	 * @param string $password
	 * @param Plugin $Plugin
	 */
	public function __construct($player, $password, $Plugin){
		parent::__construct($Plugin);
		$this->player = $player;
		$this->password = $password;
	}

	/**
	 * Get event player
	 *
	 * @return Player|OfflinePlayer
	 */
	public function getPlayer(){
		return $this->player;
	}

	/**
	 * Get the new password
	 *
	 * @return string $password
	 */
	public function getPassword(){
		return $this->password;
	}

	/**
	 * Set cancelled message
	 *
	 * @param string $message
	 */
	public function setCancelledMessage($message){
		PMC_Auth::getAPI()->canc_message = $message;
	}
}
