<?php


namespace PMC_Auth\Events;

use pocketmine\event\plugin\PluginEvent;
use pocketmine\Player;
use pocketmine\plugin\Plugin;
use pocketmine\event\Cancellable;

use PMC_Auth\PMC_Auth;

class SetMailEvent extends PluginEvent implements Cancellable {

	public static $handlerList = null;

	/** @var Player $player */
	private $player;
	
	/** @var string $email */
	private $email;
	
	/**
	 * @param Player $player
	 * @param string $email
	 * @param Plugin $Plugin
	 */
	public function __construct(Player $player, $email, $Plugin){
		parent::__construct($Plugin);
		$this->player = $player;
		$this->email = $email;
	}

	/**
	 * Get event player
	 *
	 * @return Player
	 */
	public function getPlayer(){
		return $this->player;
	}
	
	/**
	 * Get email (it can be hashed or not)
	 *
	 * @return string
	 */
	public function getPassword(){
		return $this->email;
	}	
	/**
	 * Set cancelled message
	 * 
	 * @param string $message
	 */
	public function setCancelledMessage($message){
		PMC_Auth::getAPI()->canc_message = $message;
	}
}
