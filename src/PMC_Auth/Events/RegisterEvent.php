<?php


namespace PMC_Auth\Events;

use pocketmine\event\plugin\PluginEvent;
use pocketmine\Player;
use pocketmine\plugin\Plugin;
use pocketmine\event\Cancellable;

use PMC_Auth\PMC_Auth;

class RegisterEvent extends PluginEvent implements Cancellable {

	public static $handlerList = null;

	/** @var Player $player */
	private $player;
	
	/** @var string $password */
	private $password;

	/**
	 * @param Player $player
	 * @param string $password
	 * @param Plugin $Plugin
	 */
	public function __construct(Player $player, $password, $Plugin){
		parent::__construct($Plugin);
		$this->player = $player;
		$this->password = $password;
	}

	/**
	 * Get event player
	 *
	 * @return Player
	 */
	public function getPlayer(){
		return $this->player;
	}
	
	/**
	 * Get password (it can be hashed or not)
	 * 
	 * @return string $password
	 */
	public function getPassword(){
		return $this->password;
	}
	
	/**
	 * Set cancelled message
	 *
	 * @param string $message
	 */
	public function setCancelledMessage($message){
		PMC_Auth::getAPI()->canc_message = $message;
	}
}
