<?php


namespace PMC_Auth\Commands;

use pocketmine\command\Command;
use pocketmine\command\CommandExecutor;
use pocketmine\command\CommandSender;
use pocketmine\Player;
use PMC_Auth\PMC_Auth;

class Unregister implements CommandExecutor {

	/** @var PMC_Auth $plugin */
	private $plugin;

	public function __construct(PMC_Auth $Plugin){
		$this->plugin = $Plugin;
	}

	public function onCommand(CommandSender $sender, Command $cmd, $label, array $args) {
		$fcmd = strtolower($cmd->getName());
		$p = $this->plugin;
		$L = $p->chlang;
		$U = $L["unregister"];
		$E = $L["errors"];
		switch($fcmd){
			case "unregister":
				if($sender->hasPermission("pmcauth.unregister")){
					//Player Sender
					if($sender instanceof Player){
						$cfg = $this->plugin->getCfg();
						//Check if unregister is enabled
						if($cfg["unregister"]["enabled"]){
							if(PMC_Auth::getAPI()->isPlayerAuthenticated($sender->getName())){
								//Check if password is required
								if($cfg["unregister"]["require-password"]){
									//Check args
									if(count($args) == 1){
										$playerData = PMC_Auth::getAPI()->selectPlayer($sender->getName());
										if($playerData["result"] == 0) {
											if (PMC_Auth::getAPI()->validatePassword($args[0], $playerData["password"])){
												$status = PMC_Auth::getAPI()->unregisterPlayer($sender);
												if ($status == PMC_Auth::SUCCESS) {
													$p->msg($sender, $U["unregister-success"]);
												} elseif ($status == PMC_Auth::ERR_USER_NOT_REGISTERED) {
													$p->msg($sender, $E["user-not-registered"]);
												} elseif ($status == PMC_Auth::CANCELLED) {
													$p->msg($sender, PMC_Auth::getAPI()->getCancelledMessage());
												} else {
													$p->msg($sender, $E["generic"]);
												}
											} else {
												$p->msg($sender, $E["wrong-password"]);
											}
										}elseif($playerData["result"] == 1) {
											$p->msg($sender, $E["user-not-registered"]);
										}else {
											$p->msg($sender, $E["generic"]);
										}	
									}else{
										$p->msg($sender,$U["command"]);
									}
								}else{
									$status = PMC_Auth::getAPI()->unregisterPlayer($sender);
									if($status == PMC_Auth::SUCCESS){
										$p->msg($sender,$U["unregister-success"]);
									}elseif($status == PMC_Auth::ERR_USER_NOT_REGISTERED){
										$p->msg($sender,$E["user-not-registered"]);
									}elseif($status == PMC_Auth::CANCELLED){
										$p->msg($sender,PMC_Auth::getAPI()->getCancelledMessage());
									}else{
										$p->msg($sender,$E["generic"]);
									}
								}
							}else{
								$p->msg($sender,$U["login-required"]);
							}
						}else{
							$p->msg($sender,$U["disabled"]);
						}
						break;
					}else{ //Console Sender
						if(isset($args[0])){
							$player = $this->plugin->getServer()->getPlayer($args[0]);
							if(!$player instanceof Player){
								$player = $args[0];
							}
							$status = PMC_Auth::getAPI()->unregisterPlayer($player);
							if($status == PMC_Auth::SUCCESS){
								$p->msg($sender,$p->replaceArrays($U["unregister-success-3rd"], array("PLAYER" => $args[0])));
							}elseif($status == PMC_Auth::ERR_USER_NOT_REGISTERED){
								$p->msg($sender,$E["user-not-registered-3rd"]);
							}elseif($status == PMC_Auth::CANCELLED){
								$p->msg($sender,PMC_Auth::getAPI()->getCancelledMessage());
							}else{
								$p->msg($sender,$E["generic"]);
							}
						}else{
							$p->msg($sender,$U["command-cons"]);
						}
						break;
					}
				}else{
					$p->msg($sender,$E["no-permissions"],"");
					break;
				}
		}
		return true;
	}
}
