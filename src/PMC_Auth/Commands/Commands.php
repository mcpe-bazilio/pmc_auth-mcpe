<?php


namespace PMC_Auth\Commands;

use pocketmine\command\Command;
use pocketmine\command\CommandExecutor;
use pocketmine\command\CommandSender;
use pocketmine\plugin\PluginBase;
use PMC_Auth\Tasks\MySQLTask;
use PMC_Auth\PMC_Auth;

class Commands extends PluginBase implements CommandExecutor {

	/** @var PMC_Auth $plugin */
	private $plugin;

	public function __construct(PMC_Auth $Plugin){
		$this->plugin = $Plugin;
	}

	public function onCommand(CommandSender $sender, Command $cmd, $label, array $args) {
		$fcmd = strtolower($cmd->getName());
		$p = $this->plugin;
		$cfg = $p->getCfg();
		$L = $p->chlang;
		$H = $L["help"];
		$E = $L["errors"];
		switch($fcmd){
			case "pmcauth":
				if(isset($args[0])){
					$args[0] = strtolower($args[0]);
					if($args[0] == "help"){
						if($sender->hasPermission("pmcauth.help")){
							$p->msg($sender,$H["1"],"");
							$p->msg($sender,$H["2"],"");
							$p->msg($sender,$H["3"],"");
							$p->msg($sender,$H["4"],"");
							$p->msg($sender,$H["5"],"");
							$p->msg($sender,$H["6"],"");
							$p->msg($sender,$H["7"],"");
							$p->msg($sender,$H["8"],"");
							$p->msg($sender,$H["9"],"");
							break;
						}else{
							$p->msg($sender,$E["no-permissions"],"");
							break;
						}
					}elseif($args[0]=="info"){
						if($sender->hasPermission("pmcauth.info")){
							$p->msg($sender,"&bPMC_Auth &av" . $cfg['version'] . " &bdeveloped by&a " . $cfg['author']);
							$p->msg($sender,"&bWebsite &a" . $cfg['website']);
							break;
						}else{
							$p->msg($sender,$E["no-permissions"],"");
							break;
						}
					}elseif($args[0]=="reload"){
						if(!$p->isPlayerAuthenticated($sender->getName())){
							$p->msg($sender, $p->replaceArrays($p->getCfgL()["command-forbidden"], ["COMMAND" => '/' . $fcmd . ' ' . $args[0]]));
							break;							
						}
						
						if($sender->hasPermission("pmcauth.reload")){
							$p->reloadConfig();
							$cfg = $p->getCfg();
							$p->chlang = PMC_Auth::getAPI()->getLanguage()->getAll();
							$L = $p->chlang;							
							//Restart MySQL
							PMC_Auth::getAPI()->task->cancel();
							$p->task = $p->getServer()->getScheduler()->scheduleRepeatingTask(new MySQLTask($p), 20);
							$p->mysql = false;
							//Check MySQL
							if($cfg["use-mysql"] == true){
								$check = $p->checkDatabase($cfg["mysql"]);
								if($check[0]){
									$p->initializeDatabase($cfg["mysql"]);
									$slog = $L["mysql-success"];
									$p->mysql = true;
								}else{
									$slog = PMC_Auth::getAPI()->replaceArrays($L["mysql-fail"], array("MYSQL_ERROR" => $check[1]));
								}
								$p->LOG($slog);
							}
							//End MySQL Restart
							$p->msg($sender,$L["config-reloaded"]);
							break;
						}else{
							$p->msg($sender,$E["no-permissions"],"");
							break;
						}
					}else{
						if($sender->hasPermission("pmcauth")){
							$p->msg($sender,PMC_Auth::getAPI()->replaceArrays($H["error"], array("SUBCMD" => $args[0])));
							break;
						}else{
							$p->msg($sender,$E["no-permissions"],"");
							break;
						}
					}
				}else{
					if($sender->hasPermission("pmcauth.help")){
						$p->msg($sender,$H["1"],"");
						$p->msg($sender,$H["2"],"");
						$p->msg($sender,$H["3"],"");
						$p->msg($sender,$H["4"],"");
						$p->msg($sender,$H["5"],"");
						$p->msg($sender,$H["6"],"");
						$p->msg($sender,$H["7"],"");
						$p->msg($sender,$H["8"],"");
						$p->msg($sender,$H["9"],"");
						break;
					}else{
						$p->msg($sender,$E["no-permissions"],"");
						break;
					}
				}
		}
	}
}
