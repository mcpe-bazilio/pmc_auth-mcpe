<?php


namespace PMC_Auth\Commands;

use pocketmine\command\Command;
use pocketmine\command\CommandExecutor;
use pocketmine\command\CommandSender;
use pocketmine\Player;
use PMC_Auth\PMC_Auth;

class Logout implements CommandExecutor {

	/** @var PMC_Auth $plugin */
	private $plugin;

	public function __construct(PMC_Auth $Plugin){
		$this->plugin = $Plugin;
	}

	public function onCommand(CommandSender $sender, Command $cmd, $label, array $args) {
		$fcmd = strtolower($cmd->getName());
		$p = $this->plugin;
		$L = $p->chlang;
		$LL = $L["logout"];
		$E = $L["errors"];
		switch($fcmd){
			case "logout":
				if($sender->hasPermission("pmcauth.logout")){
					//Player Sender
					if($sender instanceof Player){
						$cfg = $this->plugin->getCfg();
						//Check if logout is enabled
						if($cfg["logout"]["enabled"]){
							$status = PMC_Auth::getAPI()->deAuthenticatePlayer($sender);
							if($status == PMC_Auth::SUCCESS){
								$p->msg($sender,$LL["logout-success"]);
							}elseif($status == PMC_Auth::ERR_USER_NOT_AUTHENTICATED){
								$p->msg($sender,$E["user-not-authenticated"]);
							}elseif($status == PMC_Auth::ERR_USER_NOT_REGISTERED){
								$p->msg($sender,$E["user-not-registered"]);
							}elseif($status == PMC_Auth::CANCELLED){
								$p->msg($sender,PMC_Auth::getAPI()->getCancelledMessage());
							}else{
								$p->msg($sender,$E["generic"]);
							}
						}else{
							$p->msg($sender,$LL["disabled"]);
						}
						break;
					}else{ //Console Sender
						$p->msg($sender,$E["player-only"]);
						break;
					}
				}else{
					$p->msg($sender,$E["no-permissions"],"");
					break;
				}
		}
		return true;
	}
}
