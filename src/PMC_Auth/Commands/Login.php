<?php

namespace PMC_Auth\Commands;

use PMC_Auth\PMC_Auth;
use pocketmine\command\Command;
use pocketmine\command\CommandExecutor;
use pocketmine\command\CommandSender;
use pocketmine\Player;

class Login implements CommandExecutor {

	/** @var PMC_Auth $plugin */
	private $plugin;

	public function __construct(PMC_Auth $Plugin){
		$this->plugin = $Plugin;
	}

	public function onCommand(CommandSender $sender, Command $cmd, $label, array $args){
		$fcmd = strtolower($cmd->getName());
		$p = $this->plugin;
		$L = $p->chlang;

		switch($fcmd){
			case "login":
				if(!$sender instanceof Player){
					//Console Sender
					$p->msg($sender, $L["errors"]["player-only"]);
					return true;
				}
				//Check args
				if(count($args) != 1){
					$p->msg($sender, $L["login"]["command"]);
					return true;
				}
				$p->tryLogin($sender, $args[0]);
		}
		return true;
	}

}
