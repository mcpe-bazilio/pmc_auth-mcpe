<?php


namespace PMC_Auth\Commands;

use pocketmine\command\Command;
use pocketmine\command\CommandExecutor;
use pocketmine\command\CommandSender;
use pocketmine\Player;
use PMC_Auth\PMC_Auth;

class SetMail implements CommandExecutor {

	/** @var PMC_Auth $plugin */
	private $plugin;

	public function __construct(PMC_Auth $Plugin){
		$this->plugin = $Plugin;
	}

	public function onCommand(CommandSender $sender, Command $cmd, $label, array $args) {
		$fcmd = strtolower($cmd->getName());
		$p = $this->plugin;
		$L = $p->chlang;
		$S = $L["setmail"];
		$E = $L["errors"];
		switch($fcmd){
			case "setmail":
				if(!$sender->hasPermission("pmcauth.setmail")){
					$p->msg($sender,$E["no-permissions"],"");
					break;
				}					
				if(!($sender instanceof Player)){
					//Console Sender
					$p->msg($sender,$E["player-only"]);
					break;
				}
				//Player Sender

				//Check args
				if(count($args) != 1){
					$p->msg($sender,$S["wrong-args"] . "\n" . $S["command"]);
					break;
				}

				$cfg = $this->plugin->getCfg();

				$status = PMC_Auth::getAPI()->setEmail($sender, $args[0]);
				if($status == PMC_Auth::SUCCESS){
					PMC_Auth::getAPI()->authenticatePlayer($sender, $args[0]);
					$smsg = PMC_Auth::getAPI()->replaceArrays($S["setmail-success"],array("SITE" => $cfg["site-url"]));
					$p->msg($sender,$smsg);
				}elseif($status == PMC_Auth::ERR_USER_NOT_REGISTERED){
					$p->msg($sender,$E["user-not-registered"] . " " . $L["register"]["message"]);
				}elseif($status == PMC_Auth::INVALID_EMAIL){
					$p->msg($sender,$S["invalid-email"]);
				}elseif($status == PMC_Auth::NOT_IMPLEMENTED_WITHOUT_MYSQL){
					$p->msg($sender,$S["not-implemented"]);
				}elseif($status == PMC_Auth::CANCELLED){
					$p->msg($sender,PMC_Auth::getAPI()->getCancelledMessage());
				}else{
					$p->msg($sender,$E["generic"]);
				}

				break;
		}
		return true;
	}
}
