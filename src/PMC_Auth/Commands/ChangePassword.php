<?php

/*
 * PMC_Auth (v1.0) by Play-MC
 * Website: http://www.play-mc.tk
 * Date: 24/12/2016 01:10 PM (UTC)
 */
namespace PMC_Auth\Commands;

use pocketmine\command\Command;
use pocketmine\command\CommandExecutor;
use pocketmine\command\CommandSender;
use pocketmine\Player;
use PMC_Auth\PMC_Auth;

class ChangePassword implements CommandExecutor {

	/** @var PMC_Auth $plugin */
	private $plugin;
	
	public function __construct(PMC_Auth $Plugin){
        $this->plugin = $Plugin;
    }
	
    public function onCommand(CommandSender $sender, Command $cmd, $label, array $args) {
    	$fcmd = strtolower($cmd->getName());
	    $p = $this->plugin;
	    $L = $p->chlang;
	    $C = $L["changepassword"];
	    $E = $L["errors"];
	    switch($fcmd){
    		case "changepassword":
    			if($sender->hasPermission("pmcauth.changepassword")){
    				$cfg = $p->getCfg();
    				//Player Sender
    				if($sender instanceof Player){
    					//Check if changepassword is enabled
    					if($cfg["changepassword"]["enabled"]){
    						if(PMC_Auth::getAPI()->isPlayerAuthenticated($sender->getName())){
    							//Check confirm password
    							if($cfg["changepassword"]["password-confirm-required"]){
    								//Check args
    								if(count($args) == 2){
    									if($args[0] == $args[1]){
    										$status = PMC_Auth::getAPI()->changePlayerPassword($sender, $args[0]);
    										if($status == PMC_Auth::SUCCESS){
											    $p->msg($sender,$C["changepassword-success"]);
    										}elseif($status == PMC_Auth::ERR_USER_NOT_REGISTERED){
											    $p->msg($sender,$E["user-not-registered"]);
    										}elseif($status == PMC_Auth::ERR_PASSWORD_TOO_SHORT){
											    $p->msg($sender,PMC_Auth::getAPI()->replaceArrays($E["password-too-short"], array("MIN_PASS_LEN" => $cfg["min-password-length"],"MAX_PASS_LEN" => $cfg["max-password-length"])));
										    }elseif($status == PMC_Auth::ERR_PASSWORD_TOO_LONG){
											    $p->msg($sender,PMC_Auth::getAPI()->replaceArrays($E["password-too-long"], array("MIN_PASS_LEN" => $cfg["min-password-length"],"MAX_PASS_LEN" => $cfg["max-password-length"])));
										    }elseif($status == PMC_Auth::CANCELLED){
											    $p->msg($sender,PMC_Auth::getAPI()->getCancelledMessage());
    										}else{
											    $p->msg($sender,$E["generic"]);
    										}
    									}else{
										    $p->msg($sender,$E["password-no-match"]);
    									}
    								}else{
									    $p->msg($sender,$C["command-conf"]);
    								}
    							}else{
    								//Check args
    								if(count($args) == 1){
    									$status = PMC_Auth::getAPI()->changePlayerPassword($sender, $args[0]);
									    if($status == PMC_Auth::SUCCESS){
										    $p->msg($sender,$C["changepassword-success"]);
									    }elseif($status == PMC_Auth::ERR_USER_NOT_REGISTERED){
										    $p->msg($sender,$E["user-not-registered"]);
									    }elseif($status == PMC_Auth::ERR_PASSWORD_TOO_SHORT){
										    $p->msg($sender,PMC_Auth::getAPI()->replaceArrays($E["password-too-short"], array("MIN_PASS_LEN" => $cfg["min-password-length"],"MAX_PASS_LEN" => $cfg["max-password-length"])));
									    }elseif($status == PMC_Auth::ERR_PASSWORD_TOO_LONG){
										    $p->msg($sender,PMC_Auth::getAPI()->replaceArrays($E["password-too-long"], array("MIN_PASS_LEN" => $cfg["min-password-length"],"MAX_PASS_LEN" => $cfg["max-password-length"])));
									    }elseif($status == PMC_Auth::CANCELLED){
										    $p->msg($sender,PMC_Auth::getAPI()->getCancelledMessage());
									    }else{
										    $p->msg($sender,$E["generic"]);
									    }
    								}else{
									    $p->msg($sender,$C["command"]);
    								}
    							}
    						}else{
							    $p->msg($sender,$C["login-required"]);
    						}
    					}else{
						    $p->msg($sender,$C["disabled"]);
    					}
    					break;
    				}else{ //Console Sender
    					if(count($args) == 2){
    						$status = PMC_Auth::getAPI()->changePlayerPassword($this->plugin->getServer()->getOfflinePlayer($args[0]), $args[1]);
    						if($status == PMC_Auth::SUCCESS){
							    $p->msg($sender,PMC_Auth::getAPI()->replaceArrays($C["changepassword-cons-success"], array("PLAYER" => $args[0], "PASSWORD" => $args[1])));
    						}elseif($status == PMC_Auth::ERR_USER_NOT_REGISTERED){
							    $p->msg($sender,$E["user-not-registered-3rd"]);
    						}elseif($status == PMC_Auth::ERR_PASSWORD_TOO_SHORT){
							    $p->msg($sender,PMC_Auth::getAPI()->replaceArrays($E["password-too-short"], array("MIN_PASS_LEN" => $cfg["min-password-length"],"MAX_PASS_LEN" => $cfg["max-password-length"])));
						    }elseif($status == PMC_Auth::ERR_PASSWORD_TOO_LONG){
							    $p->msg($sender,PMC_Auth::getAPI()->replaceArrays($E["password-too-long"], array("MIN_PASS_LEN" => $cfg["min-password-length"],"MAX_PASS_LEN" => $cfg["max-password-length"])));
						    }elseif($status == PMC_Auth::CANCELLED){
							    $p->msg($sender,PMC_Auth::getAPI()->getCancelledMessage());
						    }else{
							    $p->msg($sender,$E["generic"]);
						    }
    					}else{
						    $p->msg($sender,$C["command-cons"]);
					    }
					    break;
				    }
			    }else{
				    $p->msg($sender,$E["no-permissions"],"");
    				break;
    			}
            }
	    return true; 
    }
}
