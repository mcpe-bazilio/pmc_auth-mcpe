<?php


namespace PMC_Auth\Commands;

use pocketmine\command\Command;
use pocketmine\command\CommandExecutor;
use pocketmine\command\CommandSender;
use pocketmine\Player;
use PMC_Auth\PMC_Auth;

class Register implements CommandExecutor {

	/** @var PMC_Auth $plugin */
	private $plugin;

	public function __construct(PMC_Auth $Plugin){
		$this->plugin = $Plugin;
	}

	public function onCommand(CommandSender $sender, Command $cmd, $label, array $args) {
		$fcmd = strtolower($cmd->getName());
		$p = $this->plugin;
		$L = $p->chlang;
		$R = $L["register"];
		$E = $L["errors"];
		switch($fcmd){
			case "register":
				if($sender->hasPermission("pmcauth.register")){
					//Player Sender
					if($sender instanceof Player){
						$cfg = $this->plugin->getCfg();
						//Check if register is enabled
						if($cfg["register"]["enabled"]){
							//Check confirm password
							if($cfg["register"]["password-confirm-required"]){
								//Check args
								if(count($args) == 2){
									if($args[0] == $args[1]){
										$status = PMC_Auth::getAPI()->registerPlayer($sender, $args[0]);
										if($status == PMC_Auth::SUCCESS){
											PMC_Auth::getAPI()->authenticatePlayer($sender, $args[0]);
											$p->msg($sender,$R["register-success"]);
										}elseif($status == PMC_Auth::ERR_USER_ALREADY_REGISTERED){
											$p->msg($sender,$R["already-registered"]);
										}elseif($status == PMC_Auth::ERR_PASSWORD_TOO_SHORT){
											$p->msg($sender,PMC_Auth::getAPI()->replaceArrays($E["password-too-short"], array("MIN_PASS_LEN" => $cfg["min-password-length"],"MAX_PASS_LEN" => $cfg["max-password-length"])));
										}elseif($status == PMC_Auth::ERR_PASSWORD_TOO_LONG){
											$p->msg($sender,PMC_Auth::getAPI()->replaceArrays($E["password-too-long"], array("MIN_PASS_LEN" => $cfg["min-password-length"],"MAX_PASS_LEN" => $cfg["max-password-length"])));
										}elseif($status == PMC_Auth::ERR_MAX_IP_REACHED){
											$p->msg($sender,$E["max-ip-reached"]);
										}elseif($status == PMC_Auth::CANCELLED){
											$p->msg($sender,PMC_Auth::getAPI()->getCancelledMessage());
										}else{
											$p->msg($sender,$E["generic"]);
										}
									}else{
										$p->msg($sender,$E["password-no-match"]);
									}
								}else{
									$p->msg($sender,$R["command-conf"]);
								}
							}else{
								//Check args
								if(count($args) == 1){
									$status = PMC_Auth::getAPI()->registerPlayer($sender, $args[0]);
									if($status == PMC_Auth::SUCCESS){
										PMC_Auth::getAPI()->authenticatePlayer($sender, $args[0]);
										$p->msg($sender,$R["register-success"]);
									}elseif($status == PMC_Auth::ERR_USER_ALREADY_REGISTERED){
										$p->msg($sender,$R["already-registered"]);
									}elseif($status == PMC_Auth::ERR_PASSWORD_TOO_SHORT){
										$p->msg($sender,PMC_Auth::getAPI()->replaceArrays($E["password-too-short"], array("MIN_PASS_LEN" => $cfg["min-password-length"],"MAX_PASS_LEN" => $cfg["max-password-length"])));
									}elseif($status == PMC_Auth::ERR_PASSWORD_TOO_LONG){
										$p->msg($sender,PMC_Auth::getAPI()->replaceArrays($E["password-too-long"], array("MIN_PASS_LEN" => $cfg["min-password-length"],"MAX_PASS_LEN" => $cfg["max-password-length"])));
									}elseif($status == PMC_Auth::ERR_MAX_IP_REACHED){
										$p->msg($sender,$E["max-ip-reached"]);
									}elseif($status == PMC_Auth::CANCELLED){
										$p->msg($sender,PMC_Auth::getAPI()->getCancelledMessage());
									}else{
										$p->msg($sender,$E["generic"]);
									}
								}else{
									$p->msg($sender,$R["command"]);
								}
							}
						}else{
							$p->msg($sender,$R["disabled"]);
						}
						break;
					}else{ //Console Sender
						$p->msg($sender,$E["player-only"]);
						break;
					}
				}else{
					$p->msg($sender,$E["no-permissions"],"");
					break;
				}
		}
		return true;
	}
}
