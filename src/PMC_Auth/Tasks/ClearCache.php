<?php


namespace PMC_Auth\Tasks;

use PMC_Auth\PMC_Auth;
use pocketmine\scheduler\PluginTask;

class ClearCache extends PluginTask {

	/** @var PMC_Auth $plugin */
	private $plugin;

	public function __construct(PMC_Auth $Plugin){
		parent::__construct($Plugin);
		$this->plugin = $this->getOwner();
	}

	public function onRun($tick){
		$p = $this->plugin;
		$c = $p->cached_registered_users;
		foreach($c as $playerLc => $playerData){
			if(!$p->isPlayerAuthenticated($playerLc) && $playerData['cache-expire'] !== null && $playerData['cache-expire'] < microtime(true)){
				unset($c[$playerLc]);
			}
		}
	}
}
