<?php


namespace PMC_Auth\Tasks;

use PMC_Auth\PMC_Auth;
use pocketmine\scheduler\PluginTask;

class MySQLTask extends PluginTask {

	/** @var PMC_Auth $plugin */
	private $plugin;

	public function __construct(PMC_Auth $Plugin){
		parent::__construct($Plugin);
		$this->plugin = $this->getOwner();
	}

	public function onRun($tick){
		$p = $this->plugin;
		$cfg = $p->getCfg();
		$mcfg = $cfg["mysql"];
		//Check MySQL
		if($cfg["use-mysql"] == true){
			if(PMC_Auth::getAPI()->getDatabase() == false){
				$check = PMC_Auth::getAPI()->checkDatabase($mcfg);
				if($check[0]){
					$p->LOG($p->chlang["mysql-restored"]);
					PMC_Auth::getAPI()->initializeDatabase($mcfg);
					PMC_Auth::getAPI()->mysql = true;
				}
			}elseif(!PMC_Auth::getAPI()->getDatabase()->ping()){
				$check = PMC_Auth::getAPI()->checkDatabase($mcfg);
				if($check[0]){
					$p->LOG($p->chlang["mysql-restored"]);
					PMC_Auth::getAPI()->initializeDatabase($mcfg);
					PMC_Auth::getAPI()->mysql = true;
				}
			}
		}else{
			PMC_Auth::getAPI()->mysql = false;
		}
	}

}
