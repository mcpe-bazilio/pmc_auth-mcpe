<?php

namespace PMC_Auth\Tasks;

use PMC_Auth\PMC_Auth;
use pocketmine\Player;
use pocketmine\scheduler\PluginTask;

class DelayedMessageTask extends PluginTask {

	/** @var PMC_Auth $plugin */
	private $plugin;
	private $player;
	private $message;

	public function __construct(PMC_Auth $Plugin, Player $player, $message){
		parent::__construct($Plugin);
		$this->plugin = $this->getOwner();
		$this->player = $player;
		$this->message = $message;
	}

	public function onRun($tick){
		$this->plugin->msg($this->player, $this->message);
	}
}
