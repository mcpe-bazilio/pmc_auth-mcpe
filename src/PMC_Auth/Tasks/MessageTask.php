<?php


namespace PMC_Auth\Tasks;

use PMC_Auth\PMC_Auth;
use pocketmine\scheduler\PluginTask;

class MessageTask extends PluginTask {

	/** @var PMC_Auth $plugin */
	private $plugin;

	/** @var array $players */
	private $players;

	public function __construct(PMC_Auth $Plugin){
		parent::__construct($Plugin);
		$this->plugin = $this->getOwner();
		$this->players = [];
	}

	public function onRun($tick){
		$p = $this->plugin;
		$cfg = $p->getCfg();
		$L = $p->chlang;
		$aP = &$this->players;
		$messageTaskInterval = $p->messageTaskInterval;
		$chkEmailInterval = $cfg["check-email"]["message-interval"];

		foreach($p->getServer()->getOnlinePlayers() as $player){
			$playerLc = strtolower($player->getName());
			$Authenticated = PMC_Auth::getAPI()->isPlayerAuthenticated($playerLc);
			$Registered = ($Authenticated || PMC_Auth::getAPI()->isPlayerRegistered($playerLc));
			$EmailNeed = ($Registered && $cfg["check-email"]["enabled"] && PMC_Auth::getAPI()->isPlayerNeedEmail($player));

			//Сообщения выводим только неавторизованным и тем, у кого нет Email
			if(!$Authenticated || $EmailNeed){
				//Если для этих пользователей еще не инициализирован кеш, инициализируем.
				if(!isset($this->players[$playerLc])){
					$aP[$playerLc]["interval"] = 0;
					$aP[$playerLc]["emailInterval"] = max($chkEmailInterval - 10, 0);
					$aP[$playerLc]["kick"] = 0;
				}
			}

			//Обрабатываются только те, у которых инициализирован кеш
			if(isset($this->players[$playerLc])){
				$aPP = &$this->players[$playerLc];
				if(!$Registered){
					//Для незарегистрированных пользователей
					$aPP["interval"] += $messageTaskInterval;
					$aPP["kick"] += $messageTaskInterval;
					if($aPP["interval"] >= $cfg["register"]["message-interval"]){
						if(PMC_Auth::getAPI()->areRegisterMessagesEnabled()){
							if($cfg["register"]["password-confirm-required"]){
								$p->msg($player, $L["register"]["message-conf"]);
							}else{
								$p->msg($player, $L["register"]["message"]);
							}
						}
						$aPP["interval"] = 0;
					}
					if($aPP["kick"] >= $cfg["timeout"]){
						$player->close("", $p->translateColors("&", $p->chlang["register"]["register-timeout"]));
						unset($this->players[$playerLc]);
					}
				}else{
					//Для Зарегистрированных пользователей
					if(!$Authenticated){
						$aPP["interval"] += $messageTaskInterval;
						$aPP["kick"] += $messageTaskInterval;
						if($aPP["interval"] >= $cfg["login"]["message-interval"]){
							if(PMC_Auth::getAPI()->areLoginMessagesEnabled()){
								$p->msg($player, $L["login"]["message"]);
							}
							$aPP["interval"] = 0;
						}
						if($aPP["kick"] >= $cfg["timeout"]){
							$player->close("", $p->translateColors("&", $p->chlang["login"]["login-timeout"]));
							unset($this->players[$playerLc]);
						}
					}else{
						//Аутентифицированный пользователь
						$aPP["kick"] = 0;
						if($EmailNeed){
							$aPP["emailInterval"] += $messageTaskInterval;
							if($aPP["emailInterval"] >= $chkEmailInterval){
								$p->msg($player, $L["setmail"]["message"] . "\n" . $L["setmail"]["command"]);
								$aPP["emailInterval"] = 0;
							}
						}else{
							unset($this->players[$playerLc]);
						}
					}

				}
			}
		}
		foreach($this->players as $key => $player){
			if($p->getServer()->getPlayer($key) == null){
				$aP[$key]["kick"] = 0;
			}
		}
	}
}
