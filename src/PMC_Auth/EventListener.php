<?php

namespace PMC_Auth;

use pocketmine\event\block\BlockBreakEvent;
use pocketmine\event\entity\EntityDamageByEntityEvent;
use pocketmine\event\entity\EntityDamageEvent;
use pocketmine\event\Listener;
use pocketmine\event\player\PlayerAchievementAwardedEvent;
use pocketmine\event\player\PlayerChatEvent;
use pocketmine\event\player\PlayerCommandPreprocessEvent;
use pocketmine\event\player\PlayerDropItemEvent;
use pocketmine\event\player\PlayerInteractEvent;
use pocketmine\event\player\PlayerItemConsumeEvent;
use pocketmine\event\player\PlayerJoinEvent;
use pocketmine\event\player\PlayerMoveEvent;
use pocketmine\event\player\PlayerPreLoginEvent;
use pocketmine\event\player\PlayerQuitEvent;
use pocketmine\Player;

class EventListener implements Listener {

	private $plugin;

	public function __construct(PMC_Auth $Plugin){
		$this->plugin = $Plugin;
	}

	public function onPreLogin(PlayerPreLoginEvent $event){
		//Restore default messages
		PMC_Auth::getAPI()->enableLoginMessages(true);
		PMC_Auth::getAPI()->enableRegisterMessages(true);
		$p = $this->plugin;
		$cfg = $p->getCfg();
		$L = PMC_Auth::getAPI()->getCfgL();
		if($cfg['force-single-auth']){
			$player = $event->getPlayer();
			$pName = $player->getName();
			$count = 0;
			foreach($this->plugin->getServer()->getOnlinePlayers() as $pl){
				if(strtolower($pl->getName()) == strtolower($pName)){
					$count++;
				}
			}
			if($count > 0){
				$player->close("", $p->translateColors("&", $L["single-auth"]), false);
				$event->setCancelled(true);
			}
			if(PMC_Auth::getAPI()->isPlayerAuthenticated($pName)){
				//IP Authentication
				if($cfg["login-by-ip"]){
					$playerdata = PMC_Auth::getAPI()->selectPlayer($pName);
					if($playerdata["result"] === true){
						if($playerdata["ip"] == $player->getAddress()){
							PMC_Auth::getAPI()->authenticatePlayer($player, $playerdata["password"], false); //в этом случае мы сразу берем хеш пароля
						}else{
							PMC_Auth::getAPI()->deAuthenticatePlayer($event->getPlayer());
						}
					}
				}else{
					PMC_Auth::getAPI()->deAuthenticatePlayer($event->getPlayer());
				}
			}
		}
	}

	public function onJoin(PlayerJoinEvent $event){
		$player = $event->getPlayer();
		$pName = $player->getName();
		$p = $this->plugin;
		$cfg = $p->getCfg();
		$L = PMC_Auth::getAPI()->getCfgL();
		if($cfg["show-join-message"]){
			$p->msgShd($player, $L["join-message"], 10);
		}
		if(PMC_Auth::getAPI()->isPlayerAuthenticated($pName) && $cfg["login-by-ip"]){
			$p->msgShd($player, $L["login"]["ip-login"], 12);
		}
		if(!PMC_Auth::getAPI()->isPlayerRegistered($pName) && PMC_Auth::getAPI()->areRegisterMessagesEnabled()){
			if($cfg["register"]["password-confirm-required"]){
				$p->msgShd($player, $L["register"]["message-conf"], 14);
			}else{
				$p->msgShd($player, $L["register"]["message"], 14);
			}
		}else{
			if(!PMC_Auth::getAPI()->isPlayerAuthenticated($pName) && PMC_Auth::getAPI()->areLoginMessagesEnabled()){
				$p->msgShd($player, $L["login"]["message"], 16);
			}
		}
	}

	public function onPlayerQuit(PlayerQuitEvent $event){
		//Free registered users cache
		if(isset(PMC_Auth::getAPI()->cached_registered_users[strtolower($event->getPlayer()->getName())])){
			unset(PMC_Auth::getAPI()->cached_registered_users[strtolower($event->getPlayer()->getName())]);
		}
	}

	public function onPlayerMove(PlayerMoveEvent $event){
		if(!$this->plugin->getCfg()["allow-move"]){
			if(!PMC_Auth::getAPI()->isPlayerAuthenticated($event->getPlayer()->getName())){
				$event->setCancelled(true);
			}
		}
	}

	public function onPlayerChat(PlayerChatEvent $event){
		if($this->plugin->getCfg()["block-chat"]){
			if(!PMC_Auth::getAPI()->isPlayerAuthenticated($event->getPlayer()->getName())){
				$event->setCancelled(true); //Cancel message
			}
			//Убираем тех получателей сообщений, кто не авторизован
			$recipients = $event->getRecipients();
			foreach($recipients as $key => $recipient){
				if($recipient instanceof Player){
					if(!PMC_Auth::getAPI()->isPlayerAuthenticated($recipient->getName())){
						unset($recipients[$key]);
					}
				}
			}
			$event->setRecipients($recipients);
		}
	}

	public function onPlayerCommand(PlayerCommandPreprocessEvent $event){
		// Для аутенцифицированных - все пропускаем
		$player = $event->getPlayer();
		$pName = $player->getName();
		$p = $this->plugin;
		// Для аутенцифицированных - все пропускаем
		if($p->isPlayerAuthenticated($pName)) return;
		$command = trim(strtolower($event->getMessage()));

		//Для неавторизованных...
		if($p->getCfg()["block-commands"]){
			//Если Команды неавторизованных блокируются...
			// то КОМАНДЫ  - только логина/регистрации/помощи				
			if($command{0} == "/"){ //команда
				if(!preg_match('%^/(login|ln|register|reg|help|pmcauth)\b%im', $command)){
					$event->setCancelled(true);
					if(preg_match('%^(/\w+)\b%im', $command, $matches)){
						$L = $p->getCfgL();
						$this->plugin->msg($player, $p->replaceArrays($L["command-forbidden"], ["COMMAND" => $matches[0]]));
						return;
					}
				}
			}
		}
		if($p->isPlayerRegistered($pName)){// Для зарегистрированных -- текст как пароль
			if($command{0} != "/"){ //не команда
				$event->setCancelled(true);
				$p->tryLogin($player, $command);
			}
		}
	}

	public function onPlayerInteract(PlayerInteractEvent $event){
		if(!PMC_Auth::getAPI()->isPlayerAuthenticated($event->getPlayer()->getName())){
			$event->setCancelled(true);
		}
	}

	public function onBlockBreak(BlockBreakEvent $event){
		if(!PMC_Auth::getAPI()->isPlayerAuthenticated($event->getPlayer()->getName())){
			$event->setCancelled(true);
		}
	}

	public function onEntityDamage(EntityDamageEvent $event){
		$player = $event->getEntity();
		if($player instanceof Player){
			if(!PMC_Auth::getAPI()->isPlayerAuthenticated($player->getName())){
				$event->setCancelled(true);
			}
		}
		if($event instanceof EntityDamageByEntityEvent){
			$damager = $event->getDamager();
			if($damager instanceof Player){
				if(!PMC_Auth::getAPI()->isPlayerAuthenticated($damager->getName())){
					$event->setCancelled(true);
				}
			}
		}
	}

	//Other Events

	public function onDropItem(PlayerDropItemEvent $event){
		if($this->plugin->getCfg()["block-all-events"]){
			if(!PMC_Auth::getAPI()->isPlayerAuthenticated($event->getPlayer()->getName())){
				$event->setCancelled(true);
			}
		}
	}

	public function onItemConsume(PlayerItemConsumeEvent $event){
		if($this->plugin->getCfg()["block-all-events"]){
			if(!PMC_Auth::getAPI()->isPlayerAuthenticated($event->getPlayer()->getName())){
				$event->setCancelled(true);
			}
		}
	}

	public function onAwardAchievement(PlayerAchievementAwardedEvent $event){
		if($this->plugin->getCfg()["block-all-events"]){
			if(!PMC_Auth::getAPI()->isPlayerAuthenticated($event->getPlayer()->getName())){
				$event->setCancelled(true);
			}
		}
	}
}
