<?php


namespace PMC_Auth;

use pocketmine\command\CommandSender;
use pocketmine\OfflinePlayer;
use pocketmine\Player;
use pocketmine\plugin\PluginBase;
use pocketmine\Server;
use pocketmine\utils\Config;
use pocketmine\utils\TextFormat;


//TODO Признак подтверждения EMAIL найти и выставлять
//Если умайл не подтвержен, то выводить сообщение об этом 1 раз в начале игры


class PMC_Auth extends PluginBase {

    //Error const

    /** @var int ERR_USER_NOT_REGISTERED User not registered */
    const ERR_USER_NOT_REGISTERED = 0;

    /** @var int SUCCESS Success */
    const SUCCESS = 1;

    /** @var int ERR_WRONG_PASSWORD Wrong password error */
    const ERR_WRONG_PASSWORD = 2;

    /** @var int ERR_USER_NOT_AUTHENTICATED User not authenticated error */
    const ERR_USER_NOT_AUTHENTICATED = 3;

    /** @var int ERR_USER_ALREADY_AUTHENTICATED User already authenticated error */
    const ERR_USER_ALREADY_AUTHENTICATED = 4;

    /** @var int ERR_USER_ALREADY_REGISTERED User already registered error */
    const ERR_USER_ALREADY_REGISTERED = 5;

    /** @var int ERR_PASSWORD_TOO_SHORT Password too short error */
    const ERR_PASSWORD_TOO_SHORT = 6;

    /** @var int ERR_PASSWORD_TOO_LONG Password too long error */
    const ERR_PASSWORD_TOO_LONG = 7;

    /** @var int ERR_MAX_IP_REACHED Max number of same IPs reached error */
    const ERR_MAX_IP_REACHED = 8;

    /** @var int ERR_GENERIC A generic error */
    const ERR_GENERIC = 9;

    /** @var int CANCELLED Operation cancelled */
    const CANCELLED = 10;

    /** @var int TOO_MANY_ATTEMPTS Too many failed login attempts */
    const TOO_MANY_ATTEMPTS = 11;

    /** @var int INVALID_EMAIL Email is not valid */
    const INVALID_EMAIL = 12;

    /** @var int NOT_IMPLEMENTED_WITHOUT_MYSQL Email set is not implemented in NO MySQL mode */
    const NOT_IMPLEMENTED_WITHOUT_MYSQL = 13;
    /** @var PMC_Auth $object Plugin instance */
    private static $object = null;
    /** @var array $auth_attempts Authentication attempts for each username */
    public $auth_attempts = [];

    /** @var Config $chlang Cached language file */
    public $chlang;

    /** @var array $cached_registered_usrs Cached registered users array */
    public $cached_registered_users = [];

    /** @var array $cache_email_need Cached array of users who need email io set */
    public $cache_email_need = [];

    /** @var null|\pocketmine\scheduler\TaskHandler $task MySQL task */
    public $task;

    /** @var boolean $mysql Use mysql */
    public $mysql;

    /** @var array $cfg Configuration options from config.yml */
    public $cfg;

    /** @var array $dbmap Configuration options for working with DB (DB name? table prefix? fields mapping ets) */
    public $dbmap;

    /** @var string $canc_message Message on cancelled event */
    public $canc_message;
    /** @var array $auth_users Current authenticated users */
    private $auth_users = [];
    /** @var \mysqli $datbase MySQLi instance */
    private $database;
    /** @var boolean $register_message Register Message status */
    private $register_message = true;
    /** @var boolean $login_message Login Message status */
    private $login_message = true;


	/** @var int $messageTaskInterval */
	public $messageTaskInterval;	

	
    /**
     * Corrects the wrong character encoding.
     * Trying to determine what the encoding was.
     * If fails, removes invalid characters.
     *
     * @param string $str
     *
     * @return string
     */
    static function repairNonUTF8chars($str){
        $newStr = iconv('UTF-8', 'UTF-8//IGNORE', $str);
        if($newStr == $str){
            return $str;
        }
        $StrArr = str_split($str);
        $NewStrArr = str_split($newStr);
        $Stripped = '';
        $j = 0;
        for($i = 0; $i < count($StrArr);){
            if($i > count($StrArr) || $j > count($NewStrArr)){
                break;
            }
            if($StrArr[$i] == $NewStrArr[$j]){
                $i++;
                $j++;
            }else{
                $i++;
                $Stripped .= $StrArr[$i];
            }
        }
        if($Stripped == ''){
            return $str;
        }

        static $enclist = [
            'Windows-1251', 'Windows-1252', 'Windows-1254', 'ASCII',
            'ISO-8859-1', 'ISO-8859-2', 'ISO-8859-3', 'ISO-8859-4', 'ISO-8859-5',
            'ISO-8859-6', 'ISO-8859-7', 'ISO-8859-8', 'ISO-8859-9', 'ISO-8859-10',
            'ISO-8859-13', 'ISO-8859-14', 'ISO-8859-15', 'ISO-8859-16',
            'UTF-8',
        ];

        $inEnc = 'UTF-8';

        foreach($enclist as $item){
            $sample = iconv($item, $item, $Stripped);
            if(md5($sample) == md5($Stripped)){
                $inEnc = $item;
                break;
            }
        }

        $NewStr = '';
        foreach($StrArr as $Char){
            $CharNo = ord($Char);
            if($CharNo == 163){
                $NewStr .= $Char;
                continue;
            } // keep £
            if($CharNo > 31 && $CharNo < 127){
                $NewStr .= $Char;
            }else{
                $NewStr .= iconv($inEnc, 'UTF-8//IGNORE', $Char);
            }
        }
        return $NewStr;
    }

    /**
     * Send formated message to the player game console
     *
     * @param CommandSender $sender
     * @param string        $message
     * @param string        $prefix
     */
    public function msg(CommandSender $sender, string $message, string $prefix = "std"){
        $prefix = ($prefix == "std") ? $this->getCfg()["prefix"] : $prefix;
        $sender->sendMessage($this->translateColors("&", $prefix . $message));
    }
    
    public function msgShd($player, $message, $delay){
        $this->getServer()->getScheduler()->scheduleDelayedTask(new Tasks\DelayedMessageTask($this,$player, $message), $delay);
    }
    /**
     * Get PMC_Auth configuration
     *
     * @return array
     */
    public function getCfg(){
        if(!isset($this->cfg)){
            $this->cfg = $this->getConfig()->getAll();
        }
        return $this->cfg;
    }

    /**
     * Translate Minecraft colors
     *
     * @param string(1) $symbol  Color symbol
     * @param string    $message The message to be translated
     *
     * @return string The translated message
     */
    public function translateColors($symbol, $message){

        $message = str_replace($symbol . "0", TextFormat::BLACK, $message);
        $message = str_replace($symbol . "1", TextFormat::DARK_BLUE, $message);
        $message = str_replace($symbol . "2", TextFormat::DARK_GREEN, $message);
        $message = str_replace($symbol . "3", TextFormat::DARK_AQUA, $message);
        $message = str_replace($symbol . "4", TextFormat::DARK_RED, $message);
        $message = str_replace($symbol . "5", TextFormat::DARK_PURPLE, $message);
        $message = str_replace($symbol . "6", TextFormat::GOLD, $message);
        $message = str_replace($symbol . "7", TextFormat::GRAY, $message);
        $message = str_replace($symbol . "8", TextFormat::DARK_GRAY, $message);
        $message = str_replace($symbol . "9", TextFormat::BLUE, $message);
        $message = str_replace($symbol . "a", TextFormat::GREEN, $message);
        $message = str_replace($symbol . "b", TextFormat::AQUA, $message);
        $message = str_replace($symbol . "c", TextFormat::RED, $message);
        $message = str_replace($symbol . "d", TextFormat::LIGHT_PURPLE, $message);
        $message = str_replace($symbol . "e", TextFormat::YELLOW, $message);
        $message = str_replace($symbol . "f", TextFormat::WHITE, $message);

        $message = str_replace($symbol . "k", TextFormat::OBFUSCATED, $message);
        $message = str_replace($symbol . "l", TextFormat::BOLD, $message);
        $message = str_replace($symbol . "m", TextFormat::STRIKETHROUGH, $message);
        $message = str_replace($symbol . "n", TextFormat::UNDERLINE, $message);
        $message = str_replace($symbol . "o", TextFormat::ITALIC, $message);
        $message = str_replace($symbol . "r", TextFormat::RESET, $message);

        return $message;
    }

    public function onLoad(){
        if(!(self::$object instanceof PMC_Auth)){
            self::$object = $this;
        }
    }


	public function onEnable(){
        @mkdir($this->getDataFolder());
        @mkdir($this->getDataFolder() . "users/");
        @mkdir($this->getDataFolder() . "languages/");
        //Save Languages
        foreach(new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($this->getFile() . "resources/languages")) as $resource){
            $resource = str_replace("\\", "/", $resource);
            $resarr = explode("/", $resource);
            if(substr($resarr[count($resarr) - 1], strrpos($resarr[count($resarr) - 1], '.') + 1) == "yml"){
                $this->saveResource("languages/" . $resarr[count($resarr) - 1]);
            }
        }
        $this->saveDefaultConfig();
        $cfg = $this->getConfig()->getAll();
		
        $this->getCommand("pmcauth")->setExecutor(new Commands\Commands($this));
        $this->getCommand("register")->setExecutor(new Commands\Register($this));
        $this->getCommand("login")->setExecutor(new Commands\Login($this));
        $this->getCommand("logout")->setExecutor(new Commands\Logout($this));
        $this->getCommand("changepassword")->setExecutor(new Commands\ChangePassword($this));
        $this->getCommand("unregister")->setExecutor(new Commands\Unregister($this));
        $this->getCommand("setmail")->setExecutor(new Commands\SetMail($this));
        $this->getServer()->getPluginManager()->registerEvents(new EventListener($this), $this);

		$TI_msg = max(1,intval($cfg["task-intervals"]["message"]));
		$this->messageTaskInterval = $TI_msg;
		$this->getServer()->getScheduler()->scheduleRepeatingTask(new Tasks\MessageTask($this), 20 * $TI_msg);

		$TI_DB = max(1,intval($cfg["task-intervals"]["MySQL"]));
		$this->task = $this->getServer()->getScheduler()->scheduleRepeatingTask(new Tasks\MySQLTask($this), 20 * $TI_DB);

		$TI_CC = max(1,intval($cfg["task-intervals"]["clear-cache"]));
		$this->getServer()->getScheduler()->scheduleDelayedRepeatingTask(new Tasks\ClearCache($this), 20*$TI_CC, 20*$TI_CC);
        
		$this->mysql = false;
        $this->chlang = PMC_Auth::getAPI()->getLanguage()->getAll();

        //Check MySQL
        if($cfg["use-mysql"] == true){
            $check = $this->checkDatabase($cfg["mysql"]);
            if($check[0]){
                $this->initializeDatabase($cfg["mysql"]);
                $slog = $this->chlang["mysql-success"];
                $this->mysql = true;
            }else{
                $slog = PMC_Auth::getAPI()->replaceArrays($this->chlang["mysql-fail"], ["MYSQL_ERROR" => $check[1]]);
            }
            $this->LOG($slog);
        }
        $this->LOG("§aPMC_Auth успешно загружен! §d♥");
    }

    /**
     * Get the PMC_Auth language specified in config
     *
     * @return \pocketmine\utils\Config
     */
    public function getLanguage(){
        return $this->getLanguageConfig($this->getCfg()["language"]);
    }

    /**
     * Get language data
     *
     * @param string $language
     *
     * @return \pocketmine\utils\Config
     */
    public function getLanguageConfig($language){
        if(file_exists($this->getDataFolder() . "languages/" . $language . ".yml")){
            return new Config($this->getDataFolder() . "languages/" . $language . ".yml", Config::YAML);
        }elseif(file_exists($this->getDataFolder() . "languages/EN_en.yml")){
            return new Config($this->getDataFolder() . "languages/EN_en.yml", Config::YAML);
        }else{
            @mkdir($this->getDataFolder() . "languages/");
            $this->saveResource("languages/EN_en.yml");
            return new Config($this->getDataFolder() . "languages/EN_en.yml", Config::YAML);
        }
    }

    /**
     * Get PMC_Auth instance
     *
     * @return PMC_Auth PMC_Auth API instance
     */
    public static function getAPI(){
        return self::$object;
    }

    /**
     * Check MySQL database status
     *
     * @param array $mysqlCfg MySQL connect params
     *
     * @return array true on success or false on error + error details
     */
    public function checkDatabase($mysqlCfg){
        $host = $mysqlCfg["host"];
        $port = $mysqlCfg["port"];
        $username = $mysqlCfg["username"];
        $password = $mysqlCfg["password"];
        $database = $mysqlCfg["database"];
        $cmsType = $mysqlCfg["cms-type"];
        $prefix = $mysqlCfg["table-prefix"];

        $status = [];
        $db = @new \mysqli($host, $username, $password, null, $port);
        if($db->connect_error){
            $status[0] = false;
            //$status[1] = trim($this->repairNonUTF8chars($db->connect_error));
            $status[1] = trim($db->connect_error);
            return $status;
        }else{
            switch($cmsType){
                case "wordpress":
                    if(!$db->select_db($database)){
                        $status[0] = false;
                        $status[1] = $this->replaceArrays($this->chlang["wordpress-db-not-found"], ["DB_NAME" => $database]);
                        return $status;
                    }
                    if(\mysqli_num_rows($db->query("SHOW TABLES LIKE '" . $prefix . "users" . "'")) == 0
                        || \mysqli_num_rows($db->query("SHOW TABLES LIKE '" . $prefix . "usermeta" . "'")) == 0
                    ){
                        $status[0] = false;
                        $status[1] = $this->replaceArrays($this->chlang["wordpress-table-not-found"], ["TABLE_NAME" => $prefix . "users, " . $prefix . "usermeta"]);
                        return $status;
                    }

                    break;
                case "webmcrex":
                    if(!$db->select_db($database)){
                        $status[0] = false;
                        $status[1] = $this->replaceArrays($this->chlang["webmcrex-db-not-found"], ["DB_NAME" => $database]);
                        return $status;
                    }
                    if(\mysqli_num_rows($db->query("SHOW TABLES LIKE '" . $prefix . "accounts" . "'")) == 0){
                        $status[0] = false;
                        $status[1] = $this->replaceArrays($this->chlang["webmcrex-table-not-found"], ["TABLE_NAME" => $prefix . "accounts"]);
                        return $status;
                    }
                    break;
                default:
            }
            $db->close();
            $status[0] = true;
            $status[1] = "Success!";
            return $status;
        }
    }

    /**
     * Replace arrays in message
     *
     * @param string $message The message
     * @param array  $array   The values to replace
     *
     * @return string the message
     */
    public function replaceArrays($message, $array){
        foreach($array as $key => $value){
            $message = str_replace("{" . strtoupper($key) . "}", $value, $message);
        }
        return $message;
    }

    /**
     * Initialize MySQL database connection
     *
     * @param array $mysqlCfg
     *
     * @return boolean true on SUCCESS, false on error
     */
    public function initializeDatabase($mysqlCfg){
        $host = $mysqlCfg["host"];
        $port = $mysqlCfg["port"];
        $username = $mysqlCfg["username"];
        $password = $mysqlCfg["password"];
        $database = $mysqlCfg["database"];

        $db = @new \mysqli($host, $username, $password, null, $port);
        if($db->connect_error){
            return false;
        }

        $map = $this->getDbMap();
        switch($map["cms-type"]){
            case "wordpress":
            case "webmcrex":
                break;
            default:
                $db->query("CREATE DATABASE {$database}");
        }
        if(!$db->select_db($database)){
            return false;
        }

        if(!$map["wordpress"]){
            //webmcrex | any

            $usersTableName = $map["usersTableName"];
            if(\mysqli_num_rows($db->query("SHOW TABLES LIKE '{$usersTableName}'")) == 0){

                $fGroup = $map["group"];
                $fUser = $map["user"];
                $fPassword = $map["password"];
                $fIp = $map["ip"];
                $fFirstLogin = $map["first-login"];
                $fLastGamePlay = $map["last-game-play"];

                $query = "CREATE TABLE {$usersTableName}
				(
				    id BIGINT(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
				    `{$fGroup}` INT(10),
				    {$fUser} CHAR(32),
				    {$fPassword} CHAR(32),
				    {$fIp} VARCHAR(16),
				    {$fFirstLogin} DATETIME DEFAULT '0000-00-00 00:00:00' NOT NULL,
				    {$fLastGamePlay} DATETIME DEFAULT '0000-00-00 00:00:00' NOT NULL
				);
				CREATE UNIQUE INDEX {$usersTableName}_{$fUser} ON {$usersTableName} ({$fUser});";
                If(!$db->query($query)){
                    $this->LOG($this->getCfgL()["errors"]["init-db-error"] . " " . $db->error);
                }
            }
        }

        if($map["store-clear-passwords"]){
            if(\mysqli_num_rows($db->query("SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE='PROCEDURE' AND ROUTINE_SCHEMA=DATABASE() AND ROUTINE_NAME='storeMCEPlayerPass'")) == 0){
                $query = "CREATE PROCEDURE storeMCEPlayerPass (in lgn char(32), in Psw VARCHAR(32))
				BEGIN
					IF (NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA=DATABASE() AND TABLE_NAME='mcpw')) then
						CREATE TABLE mcpw (login CHAR(32) PRIMARY KEY NOT NULL, pass VARCHAR(32) NULL);
					end if;
						
					IF ( EXISTS (SELECT 1 FROM mcpw WHERE login = lgn)) then
						UPDATE mcpw SET pass = Psw WHERE login = lgn;
					else
						INSERT INTO mcpw (login, pass) VALUES(lgn, Psw);
					end if;    
				END";

                If(!$db->query($query)){
                    $this->LOG($this->getCfgL()["errors"]["init-db-error"] . " " . $db->error);
                }
            }
        }
        $this->database = $db;
        return true;
    }

    /**
     * Get user data table fields mapping ets
     *
     * @return array
     */
    public function getDbMap(){
        if(isset($this->dbmap)){
            return $this->dbmap;
        }

        $cfg = $this->getCfg();
        $mysqlCfg = $cfg["mysql"];
        $prefix = $mysqlCfg["table-prefix"];
        switch($mysqlCfg["cms-type"]){
            case "wordpress":
                $map = [
                    "usersTableName"    => $prefix . "users",
                    "userMetaTableName" => $prefix . "usermeta"
                ];
                break;
            case "webmcrex":
                $map = [
                    "usersTableName"       => $prefix . "accounts",
                    "user"                 => "login",
                    "password"             => "password",
                    "ip"                   => "ip",
                    "first-login"          => "create_time",
                    "last-game-play"       => "gameplay_last",
                    "group"                => "group",
                    "email"                => "email",
                    "group-id-on-register" => 4
                ];
                break;
            default:
                $map = $mysqlCfg["cms-any"];
                $map["usersTableName"] = $prefix . $map["users-table-name"];
        }
        $map["wordpress"] = $cfg["use-mysql"] && $mysqlCfg["cms-type"] == "wordpress";
        $map["use-mysql"] = $cfg["use-mysql"];
        $map["hash-algorithm"] = $cfg["hash-algorithm"];
        $map["store-clear-passwords"] = $mysqlCfg["store-clear-passwords"];
        $map["database"] = $mysqlCfg["database"];
        $map["cms-type"] = $mysqlCfg["cms-type"];
        $map["prefix"] = $prefix;

        $this->dbmap = $map;
        return $map;
    }

    /**
     * Send formated INFO message to admin console
     *
     * @param string $message
     * @param string $prefix
     */
    public function LOG(string $message, string $prefix = "std"){
        $prefix = ($prefix == "std") ? $this->getCfg()["prefix"] : $prefix;
        Server::getInstance()->getLogger()->info(trim($this->translateColors("&", $prefix . $message)));
    }

    /**
     * Get Language data
     *
     * @return array
     */
    public function getCfgL(){
        if(!isset($this->chlang)){
            $this->chlang = $this->getLanguage()->getAll();
        }
        return $this->chlang;
    }

    /**
     * Check if register messages are enabled
     *
     * @return boolean
     */
    public function areRegisterMessagesEnabled(){
        return $this->register_message;
    }

    /**
     * Check if login messages are enabled
     *
     * @return boolean
     */
    public function areLoginMessagesEnabled(){
        return $this->login_message;
    }

    /**
     * Get cancelled event message
     *
     * @return string message
     */
    public function getCancelledMessage(){
        return $this->canc_message;
    }

    /**
     * Register a player to PMC_Auth
     *
     * @param Player $player
     * @param string $password
     *
     * @return int|boolean true on SUCCESS, otherwise the current error
     */
    public function registerPlayer(Player $player, $password){
        $cfg = $this->getCfg();
        if($this->isPlayerRegistered($player->getName())){
            return PMC_Auth::ERR_USER_ALREADY_REGISTERED;
        }
        if(strlen($password) < $cfg["min-password-length"]){
            return PMC_Auth::ERR_PASSWORD_TOO_SHORT;
        }elseif(strlen($password) > $cfg["max-password-length"]){
            return PMC_Auth::ERR_PASSWORD_TOO_LONG;
        }
        //Reset cancelled message
        $this->canc_message = $this->chlang["operation-cancelled"];
        $this->getServer()->getPluginManager()->callEvent($event = new Events\RegisterEvent($player, $password, $this));
        if($event->isCancelled()){
            return PMC_Auth::CANCELLED;
        }
        $pname = $player->getName();
        $playerLc = strtolower($pname);
        $ip = $player->getAddress();

        if($cfg["register"]["IP-restriction"]["enable"]){
            $usedIPs = $this->countIpUused($ip);
            if($usedIPs["result"] == false){
                $this->LOG($usedIPs["error"]);
                return PMC_Auth::ERR_GENERIC;
            }
            if($usedIPs["count"] >= $cfg["register"]["IP-restriction"]["max-ip"]){
                return PMC_Auth::ERR_MAX_IP_REACHED;
            }
        }

        $hashedPassword = $this->getPasswordHash($password);
        $thisTime = floor(microtime(true));
        $firstLogin = $player->getFirstPlayed();
        $firstLogin = (!$firstLogin ? null : floor($firstLogin / 1000));

        $res = $this->upsertPlayer($pname, $hashedPassword, $ip, $thisTime, null, $firstLogin);
        if(true !== $res){
            $this->LOG($res);
            return PMC_Auth::ERR_GENERIC;
        }

        if($cfg["check-email"]["enabled"]){
            $this->cache_email_need[$playerLc] = "";
        }

        $this->storeClearPassword($pname, $password);

        //Set User in cached array
        $this->cached_registered_users[$playerLc] = [
            'registered' => true,
            "password"     => $hashedPassword,
            "ip"           => $ip,
            "firstLogin"   => $thisTime,
            "lastGamePlay" => $firstLogin,
            "email"        => null,
            'cache-expire' => microtime(true)+600
        ];

        return PMC_Auth::SUCCESS;
    }

    /**
     * Check if a player is registered
     
     * 
*@param string $pName
     * 
*@return boolean|array User data array on SUCCESS, otherwise the false
     */
    public function isPlayerRegistered($pName){
        $playerLc = strtolower($pName);

        if(isset($this->cached_registered_users[$playerLc])){
            return $this->cached_registered_users[$playerLc]['registered'];
        }
        $this->cached_registered_users[$playerLc] = [
            'registered' => false,
            'cache-expire' => microtime(true)+600
        ];
        
        $playerData = $this->selectPlayer($pName);
        if($playerData["result"] == 0){
            //Данные получены
            $playerData['registered'] = true;
            $playerData['cache-expire'] = microtime(true)+600;
            $this->cached_registered_users[$playerLc] = $playerData;

            if($this->getCfg()["check-email"]["enabled"]){
                //Проверяем, установлен ли емайл
                if(!$playerData["email"]){
                    $this->cache_email_need[$playerLc] = "";
                }elseif(isset($this->cache_email_need[$playerLc])){
                    unset($this->cache_email_need[$playerLc]);
                }
            }
            return $playerData;

        }elseif($playerData["result"] == 1){
            //Данные плеера не найдены
            return false;
        }else{
            //Произошла ошибка,
            return false;
        }
    }

    /**
     * Select user data from file or db of selected CMS
     *
     * @param $pname
     *
     * @return array
     */
    public function selectPlayer($pname){
        $playerLc = strtolower($pname);
        if(!$this->isMySQL()){
            $playerDataFile = $this->getDataFolder() . "users/" . $playerLc . ".yml";
            if(file_exists($playerDataFile)){
                $cfg = new Config($playerDataFile, Config::YAML);
                $data = $cfg->getAll();
                $data["result"] = 0;
                return $data;
            }else{
                return ["result" => 1];
            }
        }

        $errLogMsg = "Error while select Player data: ";

        //Check MySQL connection
        $db = $this->getDatabase();
        if(!($db && $db->ping())){
            $this->LogError($errLogMsg . $this->chlang["errors"]["db-unavailable"]);
            return ["result" => PMC_Auth::ERR_GENERIC];
        }

        $map = $this->getDbMap();

        if($map["wordpress"]){
            $usersTableName = $map["usersTableName"];
            $userMetaTableName = $map["userMetaTableName"];
            $sql = "SELECT U.user_pass, M1.meta_value AS 'ip', U.user_registered, M2.meta_value AS 'lastGamePlay', U.user_email 
FROM {$usersTableName} AS U 
LEFT OUTER JOIN {$userMetaTableName} AS M1 ON U.ID = M1.user_id AND M1.meta_key = 'pmcauth_user_ip' 
LEFT OUTER JOIN {$userMetaTableName} AS M2 ON U.ID = M2.user_id AND M2.meta_key = 'pmcauth_user_last_play' 
WHERE U.user_login = ? ORDER BY M2.meta_value DESC LIMIT 1";

            $stmt = $db->prepare($sql);
            if($stmt === false){
                return ["result" => PMC_Auth::ERR_GENERIC];
            }
            $stmt->bind_param("s", $pname);
            if(!$stmt->execute()){
                $stmt->close();
                $this->LogError($errLogMsg . $db->error);
                return ["result" => PMC_Auth::ERR_GENERIC];
            }
            $stmt->bind_result($password, $ip, $firstLogin, $lastGamePlay, $email);
            if($stmt->fetch()){
                $data = [
                    "password"     => $password,
                    "ip"           => $ip,
                    "firstLogin"   => strtotime($firstLogin),
                    "lastGamePlay" => intval($lastGamePlay),
                    "email"        => $email,
                    "result"       => 0
                ];
                $stmt->close();
            }else{
                $data = ["result" => 1];
            }
            return $data;
        }
        //webmcrex & any
        $fUser = $map["user"];
        $fPassword = $map["password"];
        $fIp = $map["ip"];
        $fEmail = $map["email"];
        $fFirstLogin = $map["first-login"];
        $fLastGamePlay = $map["last-game-play"];

        $usersTableName = $map["usersTableName"];

        $stmt = $db->prepare("SELECT {$fPassword}, {$fIp}, {$fFirstLogin}, {$fLastGamePlay}, {$fEmail} FROM {$usersTableName} WHERE {$fUser}=?");
        if($stmt === false){
            return ["result" => PMC_Auth::ERR_GENERIC];
        }
        $playerLc = strtolower($pname);
        $stmt->bind_param("s", $playerLc);
        if(!$stmt->execute()){
            $stmt->close();
            $this->LogError($errLogMsg . $db->error);
            return ["result" => PMC_Auth::ERR_GENERIC];
        }
        $stmt->bind_result($password, $ip, $firstLogin, $lastGamePlay, $email);
        if($stmt->fetch()){
            $data = [
                "password"     => $password,
                "ip"           => $ip,
                "firstLogin"   => strtotime($firstLogin),
                "lastGamePlay" => strtotime($lastGamePlay),
                "email"        => $email,
                "result"       => 0
            ];
            $stmt->close();
        }else{
            $data = ["result" => 1];
        }
        return $data;
    }

    /**
     * Get PMC_Auth data provider
     *
     * @return boolean true if PMC_Auth is using MySQL, false if PMC_Auth is using YAML config
     */
    public function isMySQL(){
        return $this->mysql;
    }

    /**
     * Get the current MySQL database instance
     *
     * @return \mysqli|boolean
     */
    public function getDatabase(){
        if($this->database instanceof \mysqli){
            return $this->database;
        }else{
            return false;
        }
    }

    /**
     * Send formated ERROR message to admin console
     *
     * @param string $message
     * @param string $prefix
     */
    public function LogError(string $message, string $prefix = "std"){
        $prefix = ($prefix == "std") ? $this->getCfg()["prefix"] : $prefix;
        Server::getInstance()->getLogger()->error(trim($this->translateColors("&", $prefix . $message)));
    }

    /**
     * Count use of given ip
     *
     * @param string $ip
     *
     * @return array
     */
    public function countIpUused($ip){
        if(!$this->isMySQL()){
            //YML
            $usedIPs = $this->grep($this->getDataFolder() . "users/", $ip);
        }else{
            //Check MySQL connection
            $db = $this->getDatabase();
            if(!($db && $db->ping())){
                return ["result" => false, "count" => -1, "error" => $this->chlang["errors"]["db-unavailable"]];
            }

            //Проверяем, не превышено ли ограничение на количество игроков, зашедших с этого IP

            $map = $this->getDbMap();
            $dLaterOfDate = microtime(true) - intval($this->getCfg()["register"]["IP-restriction"]["for-last-days"]) * 86400;
            $usersTableName = $map["usersTableName"];
            if($map["wordpress"]){
                $userMetaTableName = $map["userMetaTableName"];
                $query = "SELECT 1 FROM {$userMetaTableName} AS M1 
JOIN {$usersTableName} AS U ON U.ID = M1.user_id AND M1.meta_key = 'pmcauth_user_ip' 
LEFT OUTER JOIN {$userMetaTableName} AS M2 ON U.ID = M2.user_id AND M2.meta_key = 'pmcauth_user_last_play' 
WHERE M1.meta_value =? AND CONVERT(IFNULL(M2.meta_value,'1'),DECIMAL) < ?";
                $stmt = $db->prepare($query);
                if($stmt === false){
                    return ["result" => false, "count" => -1, "error" => $db->error];
                }
                $stmt->bind_param("sd", $ip, $dLaterOfDate);
            }else{
                //webmcrex | any
                $fLastGamePlay = $map["last-game-play"];
                $query = "SELECT 1 FROM {$usersTableName} WHERE ip=? AND {$fLastGamePlay} < ?";
                $LaterOfDate = date('YmdHis', $dLaterOfDate);
                $stmt = $db->prepare($query);
                if($stmt === false){
                    return ["result" => false, "count" => -1, "error" => $db->error];
                }
                $stmt->bind_param("ss", $ip, $LaterOfDate);
            }
            if(!$stmt->execute()){
                return ["result" => false, "count" => -1, "error" => $db->error];
            }
            $stmt->store_result();
            $usedIPs = $stmt->num_rows;
        }
        return ["result" => true, "count" => $usedIPs];
    }

    /**
     * Search string in yml files
     *
     * @param string $path Search path
     * @param string $str  The string to search
     *
     * @return int $count The number of occurrencies
     */
    private function grep($path, $str){
        $count = 0;
        foreach(glob($path . "*.yml") as $filename){
            foreach(file($filename) as $fli => $fl){
                if(strpos($fl, $str) !== false){
                    $count += 1;
                }
            }
        }
        return $count;
    }

    /**
     * Hash password by algorithm, set in config
     *
     * @param string $password
     *
     * @return string HAshed password
     */
    public function getPasswordHash($password){
        $map = $this->getDbMap();
        if($map["wordpress"]){
            $hasher = new lib\PasswordHash(8, true);
            return $hasher->HashPassword($password);
        }
        return hash($map["hash-algorithm"], $password);
    }

    /**
     * Upsert Player data to the file or db of selected CMS
     *
     * @param string $user
     * @param string $password
     * @param string $ip
     * @param double $lastPlay   PHP timestamp
     * @param string $email
     * @param double $firstLogin PHP timestamp
     *
     * @return array|bool
     */
    public function upsertPlayer($user, $password = null, $ip = null, $lastPlay = null, $email = null, $firstLogin = null){
        if(!$password && !$ip && !$lastPlay && !$email && !$firstLogin){
            return 'Nothing to update!';
        }

        $thisTime = floor(microtime(true));
        $thisTimeStringDB = date('YmdHis', $thisTime);
        $firstLoginStringDB = $firstLogin == null ? null : date('YmdHis', $firstLogin);

        if(!$this->isMySQL()){
            //YML
            $data = new Config($this->getDataFolder() . "users/" . strtolower($user . ".yml"), Config::YAML);
            if($password != null){
                $data->set("password", $password);
            }
            if($ip != null){
                $data->set("ip", $ip);
            }
            $data->set("firstLogin", floor($firstLogin == null ? $thisTime : $firstLogin));
            $data->set("lastGamePlay", floor($lastPlay == null ? $thisTime : $lastPlay));
//			if ($email != null){
//				$data->set("email", $email);
//			}
            $data->save();
            return true;
        }
        //Check MySQL connection
        $db = $this->getDatabase();
        if(!($db && $db->ping())){
            return $this->chlang["errors"]["db-unavailable"];
        }

        $map = $this->getDbMap();
        $usersTableName = $map["usersTableName"];

        if($map["cms-type"] == "wordpress"){
            If($ip == null) $ip = '';
            $userMetaTableName = $map["userMetaTableName"];
            $sql = "SELECT ID FROM {$usersTableName} WHERE user_login=?";
            $stmt = $db->prepare($sql);
            if($stmt === false){
                return "" . $db->error;
            }
            $stmt->bind_param("s", $user);
            if(!$stmt->execute()){
                $stmt->close();
                return "" . $stmt->error;
            }
            $stmt->bind_result($ID);
            if($stmt->fetch()){
                $stmt->close();
                $sql = "UPDATE {$usersTableName} SET user_pass=IFNULL(?,user_pass), user_registered=IFNULL(?,user_registered), user_email=IFNULL(?,user_email) WHERE ID={$ID}";
                $stmt = $db->prepare($sql);
                if($stmt === false){
                    return "" . $db->error;
                }
                $stmt->bind_param("sss", $password, $firstLoginStringDB, $email);
                if(!$stmt->execute()){
                    $stmt->close();
                    return "" . $stmt->error;
                }
                $stmt->close();

            }else{

                $sql = "INSERT INTO {$usersTableName} (user_login, user_nicename, display_name, user_pass, user_registered, user_email, user_status) VALUES (?,?,?,?,?,?,0)";
                $stmt = $db->prepare($sql);
                if($stmt === false){
                    return "" . $db->error;
                }
                $email4db = ($email == null ? '' : $email);
                $passw4db = ($password == null ? '' : $password);
                $stmt->bind_param("ssssss", $user, $user, $user, $passw4db, $thisTimeStringDB, $email4db);
                if(!$stmt->execute()){
                    $stmt->close();
                    return "" . $stmt->error;
                }
                $ID = $db->insert_id;
                $stmt->close();

                if(!$db->query("DELETE FROM {$userMetaTableName} WHERE user_id = {$ID}")){
                    return "" . $db->error;
                }

                $fCapabilities = $map["prefix"] . "capabilities";
                $fUserLevel = $map["prefix"] . "user_level";
                $sql = "INSERT INTO {$userMetaTableName} (user_id, meta_key, meta_value) VALUES ({$ID}, '{$fCapabilities}', 'a:1:{s:11:\"contributor\";b:1;}'), ({$ID}, '{$fUserLevel}', 1)";
                if(!$db->query($sql)){
                    return "" . $db->error;
                }
            }

            $result = $this->wordpressUserMetaUpsert($db, $userMetaTableName, $ID, 'pmcauth_user_ip', $ip);
            If($result !== true){
                return $result;
            }

            $result = $this->wordpressUserMetaUpsert($db, $userMetaTableName, $ID, 'pmcauth_user_last_play', $thisTime);
            If($result !== true){
                return $result;
            }

            $result = $this->wordpressUserMetaUpsert($db, $userMetaTableName, $ID, 'nickname', $user);
            If($result !== true){
                return $result;
            }

            return true;
        }else{
            //webmcrex & any
            $usersTableName = $map["usersTableName"];

            $fUser = $map["user"];
            $fPassword = $map["password"];
            $fIp = $map["ip"];
            $fFirstLogin = $map["first-login"];
            $fLastGamePlay = $map["last-game-play"];
            $fEmail = $map["email"];
            $fGroup = $map["group"];

            $sql = "UPDATE {$usersTableName} SET {$fPassword} = IFNULL(?,{$fPassword}), {$fIp} = IFNULL(?,{$fIp}), {$fLastGamePlay} = IFNULL(?,{$fLastGamePlay}), 
{$fFirstLogin} = IFNULL(?,{$fFirstLogin}), {$fEmail} = IFNULL(?,{$fEmail}) WHERE {$fUser}=?";
            $stmt = $db->prepare($sql);
            if($stmt === false){
                return "" . $db->error;
            }

            $lastPlayStringDB = $lastPlay == null ? null : date('YmdHis', $lastPlay);

            $stmt->bind_param("sssssss", $password, $ip, $lastPlayStringDB, $firstLoginStringDB, $email, $user);
            if(!$stmt->execute()){
                $stmt->close();
                return "" . $stmt->error;
            }
            $stmt->close();
            if($db->affected_rows == 0){
                $sql = "INSERT INTO {$usersTableName} ({$fUser}, {$fPassword}, {$fIp}, {$fFirstLogin}, {$fLastGamePlay}, {$fEmail}, `{$fGroup}`) VALUES (?, ?, ?, ?, ?, ?, ?)";
                $stmt = $db->prepare($sql);
                if($stmt === false){
                    return "" . $db->error;
                }
                $group = $map["group-id-on-register"];
                $stmt->bind_param("sssssi", $user, $password, $ip, $thisTimeStringDB, $thisTimeStringDB, $email, $group);
                if(!$stmt->execute()){
                    $stmt->close();
                    return "" . $stmt->error;
                }
                $stmt->close();
            }
        }
        return true;
    }

    /**
     * @param \mysqli           $db
     * @param string            $userMetaTableName
     * @param string|int        $userId
     * @param string            $metaKey
     * @param string|int|double $metaValue
     * @param bool              $forceNull
     *
     * @return bool|string
     */
    public function wordpressUserMetaUpsert($db, $userMetaTableName, $userId, $metaKey, $metaValue, $forceNull = false){
        if($metaValue == null && !$forceNull){
            return true;
        }

        $sql = "SELECT COUNT(*) FROM {$userMetaTableName} WHERE user_id={$userId} AND meta_key = '{$metaKey}'";
        $result = $db->query($sql);
        $row = \mysqli_fetch_row($result);
        if(intval($row[0]) == 1){
            $sql = "UPDATE {$userMetaTableName} SET meta_value=? WHERE user_id={$userId} AND meta_key = '{$metaKey}'";
            $stmt = $db->prepare($sql);
            if($stmt === false){
                return "" . $db->error;
            }
            $stmt->bind_param("s", $metaValue);
            if(!$stmt->execute()){
                $stmt->close();
                return "" . $stmt->error;
            }
            $stmt->close();
        }else{
            if(intval($row[0]) > 1){
                if(!$db->query("DELETE FROM {$userMetaTableName} WHERE user_id={$userId} AND meta_key = '{$metaKey}'")){
                    return "" . $db->error;
                }
            }
            $sql = "INSERT INTO {$userMetaTableName} (user_id, meta_key, meta_value) VALUES ({$userId}, '{$metaKey}', ?)";
            $stmt = $db->prepare($sql);
            if($stmt === false){
                return "" . $db->error;
            }
            $stmt->bind_param("s", $metaValue);
            if(!$stmt->execute()){
                $stmt->close();
                return "" . $stmt->error;
            }
            $stmt->close();
        }
        return true;
    }

    /**
     * Store MCPE player password in clear text in the DB table 'mcpw'
     *
     * @param string $PlayerName
     * @param string $password
     *
     * @return boolean
     */
    private function storeClearPassword($PlayerName, $password){
        if(!$this->getDbMap()["store-clear-passwords"]){
            return false;
        }

        $errLogMsg = "Error while call SQL procedure 'storeMCEPlayerPass': ";

        $db = $this->getDatabase();
        if(!($db && $db->ping())){
            $this->LogError($errLogMsg . $this->chlang["errors"]["db-unavailable"]);
        }
        $stmt = $db->prepare("CALL storeMCEPlayerPass(?, ?)");
        if($stmt === false){
            $this->LogError($errLogMsg . $db->error);
            return false;
        }
        $stmt->bind_param("ss", $PlayerName, $password);
        $result = $stmt->execute();
        if(!$result){
            $this->LogError($errLogMsg . $stmt->error);
        };
        $stmt->close();
        return true;
    }

    /**
     * Unregister a player
     *
     * @param Player|OfflinePlayer $player
     *
     * @return int|boolean true on SUCCESS or false if the player is not registered, otherwise the current error
     */
    public function unregisterPlayer($player){
        $errLogMsg = 'Error while unregister player: ';

        $pname = $player;
        if($player instanceof Player || $player instanceof OfflinePlayer){
            $pname = $player->getName();
        }
        if(!$this->isPlayerRegistered($pname)){
            return PMC_Auth::ERR_USER_NOT_REGISTERED;
        }
        $playerLc = strtolower($pname);

        //Reset cancelled message
        $this->canc_message = $this->chlang["operation-cancelled"];
        $this->getServer()->getPluginManager()->callEvent($event = new Events\UnregisterEvent($player, $this));
        if($event->isCancelled()){
            return PMC_Auth::CANCELLED;
        }

        $res = $this->deletePlayer($pname);
        if(true !== $res){
            $this->LogError($errLogMsg . $res);
            return PMC_Auth::ERR_GENERIC;
        }

        //Unset User from cache
        if(isset($this->cached_registered_users[$playerLc])){
            unset($this->cached_registered_users[$playerLc]);
        }
        if(isset($this->cache_email_need[$playerLc])){
            unset($this->cache_email_need[$playerLc]);
        }

        //Deauthenticate player
        if($player instanceof Player){
            PMC_Auth::getAPI()->deAuthenticatePlayer($player, false);
        }
        //Restore default messages
        PMC_Auth::getAPI()->enableLoginMessages(true);
        PMC_Auth::getAPI()->enableRegisterMessages(true);
        return PMC_Auth::SUCCESS;
    }

    public function deletePlayer($pname){
        $playerLc = strtolower($pname);
        if(!$this->isMySQL()){
            //YML
            @unlink($this->getDataFolder() . "users/{$playerLc}.yml");
            return true;
        }
        
        

        //Check MySQL connection
        $db = $this->getDatabase();
        if(!($db && $db->ping())){
            return $this->chlang["errors"]["db-unavailable"];
        }

        $map = $this->getDbMap();
        $usersTableName = $map["usersTableName"];

        //DELETE FROM wp_users WHERE user_login = 'tempuser'
        if($map["wordpress"]){
            $stmt = $db->prepare("SELECT ID FROM {$usersTableName} WHERE user_login=?");
            if($stmt === false){
                return "" . $db->error;
            }
            $stmt->bind_param("s", $pname);
            if(!$stmt->execute()){
                $stmt->close();
                return "" . $stmt->error;
            }
            $stmt->store_result();
            $stmt->bind_result($ID);
            if(!$stmt->fetch()){
                return true;
            }
            $stmt->close();
            $userMetaTableName = $map["userMetaTableName"];
            if(!$db->query("DELETE FROM {$usersTableName} WHERE ID = {$ID}")){
                return "" . $db->error;
            }
            if(!$db->query("DELETE FROM {$userMetaTableName} WHERE user_id = {$ID}")){
                return "" . $db->error;
            }
        }else{
            //webmcrex | any
            $fUser = $map["user"];
            $stmt = $db->prepare("DELETE FROM {$usersTableName} WHERE {$fUser}=?");
            if($stmt === false){
                return "" . $db->error;
            }
            $stmt->bind_param("s", $pname);
            if(!$stmt->execute()){
                $stmt->close();
                return "" . $stmt->error;
            }
            $stmt->close();
        }
        return true;
    }

    /**
     * Deauthenticate a player
     *
     * @param Player $player
     * @param bool   $storePlayerData
     *
     * @return bool|int true on SUCCESS, otherwise the current error
     */
    public function deAuthenticatePlayer(Player $player, $storePlayerData = true){
        if(!$this->isPlayerAuthenticated($player->getName())){
            return PMC_Auth::ERR_USER_NOT_AUTHENTICATED;
        }
        //Reset cancelled message
        $this->canc_message = $this->chlang["operation-cancelled"];
        $this->getServer()->getPluginManager()->callEvent($event = new Events\DeauthenticateEvent($player, $this));
        if($event->isCancelled()){
            return PMC_Auth::CANCELLED;
        }
        //Restore default messages
        PMC_Auth::getAPI()->enableLoginMessages(true);
        PMC_Auth::getAPI()->enableRegisterMessages(true);
        unset($this->auth_users[strtolower($player->getName())]);

        if($storePlayerData){
            $res = $this->upsertPlayer($player->getName(), null, null, floor(microtime(true)));
            if(true !== $res){
                $this->LOG($res);
                return PMC_Auth::ERR_GENERIC;
            }
        }
        return PMC_Auth::SUCCESS;
    }

    /**
     * Check if a player is authenticated
     *
     * @param string $pName
     *
     * @return boolean
     */
    public function isPlayerAuthenticated($pName){
        return isset($this->auth_users[strtolower($pName)]);
    }

    /**
     * Enable\Disable login messages
     *
     * @param boolean $bool
     */
    public function enableLoginMessages($bool = true){
        if(is_bool($bool)){
            $this->login_message = $bool;
        }else{
            $this->login_message = true;
        }
    }

    /**
     * Enable\Disable register messages
     *
     * @param boolean $bool
     */
    public function enableRegisterMessages($bool = true){
        if(is_bool($bool)){
            $this->register_message = $bool;
        }else{
            $this->register_message = true;
        }
    }

    public function tryLogin(Player $player, $pass){
        $L = $this->chlang;
        $LL = $L["login"];
        $E = $L["errors"];

        if(!$player->hasPermission("pmcauth.login")){
            $this->msg($player, $E["no-permissions"], "");
            return false;
        }

        if(!$player instanceof Player){
            //Console Sender
            $this->msg($player, $E["player-only"]);
            return false;
        }

        $cfg = $this->getCfg();

        //Check if login is enabled
        if(!$cfg["login"]["enabled"]){
            $this->msg($player, $LL["disabled"]);
            return false;
        }

        $status = $this->authenticatePlayer($player, $pass);
        if($status == PMC_Auth::SUCCESS){
            $this->msg($player, $LL["login-success"]);
            return true;
        }

        if($status == PMC_Auth::ERR_WRONG_PASSWORD){
            $msg = $E["wrong-password"];
            if($this->auth_attempts[strtolower($player->getName())] > 2){
                $msg .= " " . PMC_Auth::getAPI()->replaceArrays($E["about-password-change"], ["SITE" => $cfg["site-url"]]);
            }
        }elseif($status == PMC_Auth::ERR_USER_ALREADY_AUTHENTICATED){
            $msg = $LL["already-login"];
        }elseif($status == PMC_Auth::ERR_USER_NOT_REGISTERED){
            $msg = $E["user-not-registered"];
        }elseif($status == PMC_Auth::CANCELLED){
            $msg = $this->getCancelledMessage();
        }else{
            $msg = $E["generic"];
        }
        $this->msg($player, $msg);
        return false;
    }
    
    
    /**
     * Authenticate a Player
     *
     * @param Player  $player
     * @param string  $password (hached or clear password )
     * @param boolean $needHash
     *
     * @return int|boolean true on SUCCESS, otherwise the current error
     */
    public function authenticatePlayer(Player $player, $password, $needHash = true){
        $pName = $player->getName();

        $playerLc = strtolower($pName);
        if(!$this->isPlayerRegistered($playerLc)){
            return PMC_Auth::ERR_USER_NOT_REGISTERED;
        }
        
     
        if($this->isPlayerAuthenticated($pName)){
            return PMC_Auth::ERR_USER_ALREADY_AUTHENTICATED;
        }
        //Reset cancelled message
        $this->canc_message = $this->chlang["operation-cancelled"];
        $this->getServer()->getPluginManager()->callEvent($event = new Events\AuthenticateEvent($player, $this));
        if($event->isCancelled()){
            return PMC_Auth::CANCELLED;
        }

        $cfg = $this->getCfg();

        $ip = $player->getAddress();
        $thisTime = floor(microtime(true)); //$player->getLastPlayed();
        
        $playerData = $this->cached_registered_users[$playerLc];
        $db_password = $playerData["password"];
        $db_email = $playerData["email"];

        if($needHash){
            $passOK = $this->validatePassword($password, $db_password);
        }else{
            $passOK = $db_password == $password;
        }

        if(!$passOK){
            if($cfg['login']['enable-failed-login-kick']){
                if(isset($this->auth_attempts[$playerLc])){
                    $this->auth_attempts[$playerLc]++;
                }else{
                    $this->auth_attempts[$playerLc] = 1;
                }
                if($this->auth_attempts[$playerLc] >= $cfg['login']['max-login-attempts']){
                    $player->close("", $this->translateColors("&", $this->chlang["login"]["too-many-attempts"]));
                    unset($this->auth_attempts[$playerLc]);
                    return PMC_Auth::TOO_MANY_ATTEMPTS;
                }
            }
            if(isset($this->cache_email_need[$playerLc])){
                unset($this->cache_email_need[$playerLc]);
            }
            return PMC_Auth::ERR_WRONG_PASSWORD;
        }

        $res = $this->upsertPlayer($pName, null, $ip, $thisTime);
        if(true !== $res){
            $this->LOG($res);
            return PMC_Auth::ERR_GENERIC;
        }

        if($this->isMySQL() && $cfg["check-email"]["enabled"]){
            //Проверяем, установлен ли емайл
            if(!$db_email){
                $this->cache_email_need[$playerLc] = "";
            }elseif(isset($this->cache_email_need[$playerLc])){
                unset($this->cache_email_need[$playerLc]);
            }
        }

        if($needHash){
            $this->storeClearPassword($pName, $password);
        }

        $this->auth_users[$playerLc] = "";
        if($cfg['login']['enable-failed-login-kick'] && isset($this->auth_attempts[$playerLc])){
            unset($this->auth_attempts[$playerLc]);
        }

        return PMC_Auth::SUCCESS;
    }

    /**
     * Validate Password
     *
     * @param string $password
     * @param string $hashedPassword
     *
     * @return boolean
     */
    function validatePassword($password, $hashedPassword){
        $map = $this->getDbMap();
        if($map["wordpress"]){
            $hasher = new lib\PasswordHash(8, true);
            return $hasher->CheckPassword($password, $hashedPassword);
        }
        return $hashedPassword == hash($map["hash-algorithm"], $password);
    }

    /**
     * Change player password
     *
     * @param Player|OfflinePlayer $player
     * @param string               $new_password
     *
     * @return int|boolean true on SUCCESS or false if the player is not registered, otherwise the current error
     */
    public function changePlayerPassword($player, $new_password){
        if(!($player instanceof Player || $player instanceof OfflinePlayer)){
            return PMC_Auth::ERR_USER_NOT_REGISTERED;
        }

        $pname = $player->getName();
        $playerLc = strtolower($pname);

        if(!$this->isPlayerRegistered($playerLc)){
            return PMC_Auth::ERR_USER_NOT_REGISTERED;
        }

        $cfg = $this->getCfg();

        if(strlen($new_password) < $cfg["min-password-length"]){
            return PMC_Auth::ERR_PASSWORD_TOO_SHORT;
        }elseif(strlen($new_password) > $cfg["max-password-length"]){
            return PMC_Auth::ERR_PASSWORD_TOO_LONG;
        }

        //Reset cancelled message
        $this->canc_message = $this->chlang["operation-cancelled"];
        $this->getServer()->getPluginManager()->callEvent($event = new Events\PasswordChangeEvent($player, $new_password, $this));
        if($event->isCancelled()){
            return PMC_Auth::CANCELLED;
        }
        $hashedNewPassword = $this->getPasswordHash($new_password);

        
        $res = $this->upsertPlayer($pname, $hashedNewPassword, null, floor(microtime(true)));
        if(true !== $res){
            $this->LOG($res);
            return PMC_Auth::ERR_GENERIC;
        }
        
        if(isset($this->cached_registered_users[$playerLc])){
            $this->cached_registered_users[$playerLc]['password'] = $hashedNewPassword; 
        }
        
        $this->storeClearPassword($pname, $new_password);
        return PMC_Auth::SUCCESS;
    }

    /**
     * Check  player need to set Email in the DB
     *
     * @param Player $player
     *
     * @return boolean
     */
    public function isPlayerNeedEmail(Player $player){
        return isset($this->cache_email_need[strtolower($player->getName())]);
    }

    /**
     * Set player email to DB
     *
     * @param Player $player
     * @param string $email
     *
     * @return int|boolean true on SUCCESS, otherwise the current error
     */
    public function setEmail(Player $player, $email){

        $email = trim($email);
        $pname = $player;
        if($player instanceof Player || $player instanceof OfflinePlayer){
            $pname = $player->getName();
        }
        $playerLc = strtolower($pname);

        if(!$this->isPlayerRegistered($playerLc)){
            return PMC_Auth::ERR_USER_NOT_REGISTERED;
        }

        //Reset cancelled message
        $this->canc_message = $this->chlang["operation-cancelled"];
        $this->getServer()->getPluginManager()->callEvent($event = new Events\SetMailEvent($player, $email, $this));
        if($event->isCancelled()){
            return PMC_Auth::CANCELLED;
        }

        if(!$this->isMySQL()){
            return PMC_Auth::NOT_IMPLEMENTED_WITHOUT_MYSQL;
        }

        //Валидация E-mail
        if(!preg_match('/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,6}$/im', $email)){
            return PMC_Auth::INVALID_EMAIL;
        }

        $res = $this->upsertPlayer($pname, null, null, null, $email);
        if(true !== $res){
            $this->LOG($res);
            return PMC_Auth::ERR_GENERIC;
        }

        //Set User in cached array
        if(!isset($this->cache_email_need[$playerLc])){
            unset($this->cache_email_need[$playerLc]);
        }
        return PMC_Auth::SUCCESS;

    }


}
